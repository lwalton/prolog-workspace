/* -*- Mode:Prolog; coding:iso-8859-1; indent-tabs-mode:nil; prolog-indent-width:8; prolog-paren-indent:3; tab-width:8; -*- */

/*exercise 1.3  */

man(vincent). 
man(jules). 
woman(mia). 
person(X):-  man(X);  woman(X). 
loves(X,Y):-  father(X,Y). 
father(Y,Z):-  man(Y),  son(Z,Y). 
father(Y,Z):-  man(Y),  daughter(Z,Y).
son(jules, vincent).
daughter(mia, vincent).

/*exercise 1.4 */

killer(butch).
married(mia, marsellus).
dead(zed).
footmassage(vincent).
footmassage(jules).
gooddancer(vincent).
gooddancer(mia).
nutritious(vegetables).
tasty(bigkahunaburger).

kills(marsellus, X):- footmassage(X).
loves(mia, X):- gooddancer(X).
eats(jules, X):- nutritious(X); tasty(X).