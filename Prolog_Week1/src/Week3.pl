numeral(0). 
numeral(succ(X)) :- numeral(X).

add(0,X,X).
add(succ(X),Y,succ(Z))  :- add(X,Y,Z).

double(X,Z):- add(X,X,Z).

doubleR(0,0).
doubleR(succ(X),succ(succ(Y))) :- doubleR(X,Y).

directlyIn(katarina, olga).
directlyIn(olga, natasha).
directlyIn(natasha, irina).

in(X,Y) :- directlyIn(X,Y).
in(X,Y) :- directlyIn(X,Z), in(Z,Y).

directTrain(saarbruecken,dudweiler). 
directTrain(forbach,saarbruecken). 
directTrain(freyming,forbach). 
directTrain(stAvold,freyming). 
directTrain(fahlquemont,stAvold). 
directTrain(metz,fahlquemont). 
directTrain(nancy,metz).

travelFromTo(X,Y):- directTrain(X,Y).
travelFromTo(X,Y):- directTrain(X,Z), travelFromTo(Z,Y).