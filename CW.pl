% Program: ROYAL

parent(queenmother,elisabeth). 
parent(elisabeth,charles).
parent(elisabeth,andrew). 
parent(elisabeth,anne).
parent(elisabeth,edward). 
parent(diana,william).
parent(diana,harry). 
parent(sarah,beatrice).
parent(anne,peter). 
parent(anne,zara).
parent(george,elisabeth). 
parent(philip,charles).
parent(philip,andrew). 
parent(philip,edward).
parent(charles,william). 
parent(charles,harry).
parent(andrew,beatrice). 
parent(andrew,eugene).
parent(mark,peter). 
parent(mark,zara).
parent(william,georgejun). 
parent(kate,georgejun).
parent(kate,charlotte).

% a.1-----------------------------

%the_royal_females([queenmother,elisabeth,diana,anne,beatrice,kate,charlotte,sarah,zara,eugene]).

the_royal_females(queenmother).
the_royal_females(elisabeth).
the_royal_females(diana).
the_royal_females(anne).
the_royal_females(beatrice).
the_royal_females(kate).
the_royal_females(charlotte).
the_royal_females(sarah).
the_royal_females(zara).
the_royal_females(eugene).

% a.2-----------------------------

%the_royal_males([andrew,edward,harry,peter,william,charles,mark,george,philip,georgejun]).

the_royal_males(andrew).
the_royal_males(edward).
the_royal_males(harry).
the_royal_males(peter).
the_royal_males(william).
the_royal_males(charles).
the_royal_males(mark).
the_royal_males(george).
the_royal_males(philip).
the_royal_males(georgejun).

% a.3-----------------------------

the_royal_family([queenmother,elisabeth,diana,anne,beatrice,kate,charlotte,sarah,zara,eugene,andrew,edward,harry,peter,william,charles,mark,george,philip,georgejun]).

the_royal_family(queenmother).
the_royal_family(elisabeth).
the_royal_family(diana).
the_royal_family(anne).
the_royal_family(beatrice).
the_royal_family(kate).
the_royal_family(charlotte).
the_royal_family(sarah).
the_royal_family(zara).
the_royal_family(eugene).
the_royal_family(andrew).
the_royal_family(edward).
the_royal_family(harry).
the_royal_family(peter).
the_royal_family(william).
the_royal_family(charles).
the_royal_family(mark).
the_royal_family(george).
the_royal_family(philip).
the_royal_family(georgejun).

% a.4-----------------------------

mother(X,Y):-  the_royal_females(X), parent(X,Y).

% a.5-----------------------------

has_child(X):- parent(X,_).

% a.6 -----------------------------

ancestor(X,Y):- parent(X, Y).
ancestor(X,Y):- parent(X,Z), ancestor(Z,Y).

% a.7 -----------------------------

descendent(X,Y):- parent(Y,X).
descendent(X,Y):- parent(Z,X), descendent(Z,Y).

% a.8 -----------------------------

%mother(X,beatrice).

%sarah

% a.9 ----------------------------

%has_child(X).

%a. 10 ---------------------------

%descendent(X,queenmother):-

% b ------------------------------

sibling(X,Y):-parent(Z,X), parent(Z,Y).

aunt(X,Y) :- parent(Z,Y), sibling(Z,X).


%c -------------------------------

palindrome_list([]).
palindrome_list([L]):- 

last_element(L,X,R):- 

%d -------------------------------

euclidsqr(X,Y,ED):- 

euclidsqr_acc(X,Y,A,ED)