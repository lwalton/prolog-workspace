%----------------------------Question1

parent(queenmother,elisabeth). 	parent(elisabeth,charles).
parent(elisabeth,andrew). 		parent(elisabeth,anne).
parent(elisabeth,edward).		parent(diana,william).
parent(diana,harry). 			parent(sarah,beatrice).
parent(anne,peter). 			parent(anne,zara).
parent(george,elisabeth). 		parent(philip,charles).
parent(philip,andrew). 			parent(philip,edward).
parent(charles,william). 		parent(charles,harry).
parent(andrew,beatrice). 		parent(andrew,eugene).
parent(mark,peter). 			parent(mark,zara).
parent(william,georgejun). 		parent(kate,georgejun).
parent(william,charlotte). 		parent(kate,charlotte).
parent(phillip,anne).

%Q1.1
the_royal_females(queenmother).
the_royal_females(elisabeth).
the_royal_females(anne).
the_royal_females(diana).
the_royal_females(sarah).
the_royal_females(beatrice).
the_royal_females(zara).
the_royal_females(eugene).
the_royal_females(kate).
the_royal_females(charlotte).
 
%Q1.2
the_royal_males(charles).
the_royal_males(andrew).
the_royal_males(edward).
the_royal_males(william).
the_royal_males(harry).
the_royal_males(peter).
the_royal_males(george).
the_royal_males(phillip).
the_royal_males(philip).
the_royal_males(mark).
the_royal_males(georgejun).
 
%Q1.3
the_royal_family(queenmother).
the_royal_family(elisabeth).
the_royal_family(charles).
the_royal_family(andrew).
the_royal_family(anne).
the_royal_family(edward).
the_royal_family(diana).
the_royal_family(william).
the_royal_family(harry).
the_royal_family(sarah).
the_royal_family(beatrice).
the_royal_family(peter).
the_royal_family(zara).
the_royal_family(george).
the_royal_family(philip).
the_royal_family(eugene).
the_royal_family(mark).
the_royal_family(georgejun).
the_royal_family(kate).
the_royal_family(phillip).
the_royal_family(charlotte).



%Q1.4
ancestor(X,Y):- 
	parent(X,Y).
	
ancestor(X,Y):- 
	ancestor(Z,Y), 
	parent(X,Z).

%Q1.5
sibiling(X,Y):- 
	parent(Z,X),
	parent(Z,Y).

%Q1.6

brother(X,Y):-
	parent(Z,X), 
	parent(Z,Y), 
	the_royal_males(X), 
	the_royal_males(Y).
	
%Q1.7 
%%	X = william ? ;
%%	X = kate ? ;
%%	X = diana ? ;
%%	X = charles ? ;
%%	X = elisabeth ? ;
%%	X = philip ? ;
%%	X = queenmother ? ;
%%	X = george ?



%Q1.8

%% decendent of Anne
%%	1 ?- ancestor(X, anne).
%%	X = elisabeth ;
%%	X = phillip ;
%%	X = queenmother ;
%%	X = george ;

%% decendent of Anne
%%	2 ?- ancestor(X, edward).
%%	X = elisabeth ;
%%	X = philip ;
%%	X = queenmother ;
%%	X = george ;

%% therfore common decendent of Anne and Edward are 
%%	elisabeth
%%	philip
%%	queenmother
%%	george
	
	
	
Q1.9

%%  queries carried out
%%	find all brothers	
%%	for all results 
%%	check if any of them have 2 decendent 
%%	if yes the query becomes true 
	
%%	edward and Andrew is the answers
%%  as they are brothers of charles and charles is an ancestor of 2


%-------------------------------------Question2

%(a)
nth_elt(1,[E|R],E,R).
nth_elt(N,[H|T],E,[H|R]) :-
   N>1,
   N1 is N-1,
   nth(N1,T,E,R).
   
%(b)
kth_sorted_elt(X,[X|_],1).
kth_soted_elt(X,[_|L],K) :- K > 1, K1 is K - 1, element_at(X,L,K1). 

%(c)
median(L, M) :- 
    member(M, L), 
    partition(L, M, A, B), 
	length(A, X), 
	length(B, X).
%----------------------------------------Question3

%(a)
euclidsqr([],[],0).
euclidsqr([X|M],[Y|N],ED) :- length([X|M],A),
                             length([Y|N],B),
                             A==B,
                             euclidsqr(M,N,Enew), ED is Enew+((X-Y)*(X-Y)).

%-------------------------------result!
%|:-euclidsqr([1,2,3],[3,4,5,6],X).
%false.
%:-|euclidsqr([4,5,6],[1,2,3],X).
%X = 27.

%(b)
euclidsqr_acc([],[],ED,ED).
euclidsqr_acc([X|M],[Y|N],Acc,ED) :- length([X|M],A),
                                     length([Y|N],B),
                                     A==B,
                                     Anew is Acc+((X-Y)*(X-Y)),
                                     euclidsqr_acc(M,N,Anew,ED).
euclidsqr_new(X,Y,ED):-euclidsqr_acc(X,Y,0,ED).

%result
%:-euclidsqr_new([1,2,3],[1,2,3,4],X).
%false.
%|:-euclidsqr_new([4,5,6],[1,2,3],X).
%X = 27.

%--------------------------------------Question4

%(a) member_rem
member_rem(E,[E|H],H).
member_rem(E,[X|H],[X|R]) :- member_rem(E,H,R).

%----------------------result!

%|:-member_rem(3,[1,2,3,4],X).
%X = [1, 2, 4]                                                

%(b) gen_list
gen_list(0,_,[]).
gen_list(N,L,[D|Ds]) :- N>0,
                        N1 is N-1,
                        member_rem(D,L,L1),
                        gen_list(N1,L1,Ds).
gen1(N,L) :- gen_list(N,[1,3,4,5,6,8,9],L).
gen2(N,L) :- gen_list(N,[1,2,3,4,6,8,9],L).

%---------------result(many answers)
%:-|gen1(7,L).
%L = [1, 3, 4, 5, 6, 8, 9] :-;
%L = [1, 3, 4, 5, 6, 9, 8] |;
%L = [1, 3, 4, 5, 8, 6, 9] |;
%L = [1, 3, 4, 5, 8, 9, 6] |;
%L = [1, 3, 4, 5, 9, 6, 8] |;
%L = [1, 3, 4, 5, 9, 8, 6] |;
%L = [1, 3, 4, 6, 5, 8, 9] |;
%L = [1, 3, 4, 6, 5, 9, 8] |;
%L = [1, 3, 4, 6, 8, 5, 9] |;
%L = [1, 3, 4, 6, 8, 9, 5] |;
%L = [1, 3, 4, 6, 9, 5, 8] |

%------------(C) solve sujiko1 & solve sujiko2
%(1) solve sujiko1
solve1([X1,X2,X3,X4,X5,X6,X7]) :- 23 is X1+X2+X4+X5,
                                  27 is X2+X3+X5+X6,
                                  17 is X4+X5+X7+7,
                                  17 is X5+X6+X7+2.
solve_suijiko1(L):- gen1(7,L),
                    solve1(L).

%------------------result
%|:-solve_suijiko1(L).
%L = [5, 9, 4, 3, 6, 8, 1] :-;
%false.

%(2) solve sujiko2                                         
solve2([X1,X2,X3,X4,X5,X6,X7]) :- 21 is X1+X2+5+X3,
                                  22 is X2+7+X3+X4,
                                  24 is 5+X3+X5+X6,
                                  14 is X3+X4+X6+X7.
solve_suijiko2(L):- gen2(7,L),
                    solve2(L).

%------------------result
%:-|solve_suijiko2(L).
%L = [2, 8, 6, 1, 9, 4, 3] :-;
%false.